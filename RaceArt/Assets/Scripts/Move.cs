﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
    
public class Move : MonoBehaviour
{   
    [HideInInspector]
    public Rigidbody Rb;
    Animator animator;
    public float speed = 15;

    public int minSwipeRecognition = 500;

    private bool isTraveling;
    private Vector3 travelDirection;

    private Vector2 swipePosLastFrame;
    private Vector2 swipePosCurrentFrame;
    private Vector2 currentSwipe;

    private Vector3 nextCollisionPosition;

    private void OnEnable()
    {
        animator = GetComponent<Animator>();
        Rb = GetComponent<Rigidbody>();
        Rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
        GetComponent<TrailRenderer>().material = GetComponent<MeshRenderer>().material;
        //To Stop Hovering
        Invoke("ShutDown", 0.25f);
    }

    private void OnDisable()
    {
        nextCollisionPosition = Vector3.zero;
        isTraveling = false;
    }

    private void FixedUpdate()
    {
        Debug.DrawLine(nextCollisionPosition, transform.position);
        if (nextCollisionPosition != Vector3.zero)
        {
            if (Vector3.Distance(transform.position, nextCollisionPosition) <= 0.5f)
            {
                PlayAnim(travelDirection);
                transform.position = new Vector3(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y), Mathf.RoundToInt(transform.position.z));
                Rb.constraints = RigidbodyConstraints.FreezeAll;
                isTraveling = false;
                travelDirection = Vector3.zero;
                nextCollisionPosition = Vector3.zero;
                this.enabled = false;
            }
        }

        if (isTraveling)
        {
            Rb.velocity = travelDirection * speed;
        }

        // Check if we have reached our destination

        if (isTraveling)
            return;

        // Swipe mechanism
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            // Where is the mouse now?
            swipePosCurrentFrame = touch.position;

            if (swipePosLastFrame != Vector2.zero)
            {
                // Calculate the swipe direction
                currentSwipe = swipePosCurrentFrame - swipePosLastFrame;

                if (currentSwipe.sqrMagnitude < minSwipeRecognition) // Minium amount of swipe recognition
                    return;

                currentSwipe.Normalize(); // Normalize it to only get the direction not the distance (would fake the balls speed)

                // Up/Down swipe
                if (currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
                {
                    SetDestination(currentSwipe.y > 0 ? Vector3.forward : Vector3.back);
                }

                // Left/Right swipe
                if (currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
                {
                    SetDestination(currentSwipe.x > 0 ? Vector3.right : Vector3.left);
                }
            }


            swipePosLastFrame = swipePosCurrentFrame;
        }

        if (Input.touchCount < 1)
        {
            swipePosLastFrame = Vector2.zero;
            currentSwipe = Vector2.zero;
        }
    }

    private void SetDestination(Vector3 direction)
    {
        travelDirection = direction;

        // Check with which object we will collide
        RaycastHit hit;
        if (Physics.Raycast(transform.position, direction, out hit, 100f, ~0, QueryTriggerInteraction.Ignore))
        {
            nextCollisionPosition = hit.point;
        }

        isTraveling = true;
    }

    private void ShutDown()
    {
        transform.position = new Vector3(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y), Mathf.RoundToInt(transform.position.z));
        Rb.constraints = RigidbodyConstraints.FreezeAll;
        this.enabled = false;
    }

    private void PlayAnim(Vector3 Dir)
    {   
        //Anim Not Working Yet
        //if(Dir == Vector3.up) { animator.SetTrigger("Up"); }
        //else if (Dir == Vector3.down) { animator.SetTrigger("Down"); }
        //else if (Dir == Vector3.right) { animator.SetTrigger("Right"); }
        //else if (Dir == Vector3.left) { animator.SetTrigger("Left"); }
    }
}
