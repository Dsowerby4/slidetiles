﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionManager : MonoBehaviour
{
    public Animator Cam;
    public Canvas Main;
    void Start()
    {   
        Cam = Camera.main.GetComponent<Animator>();
    }

    public void StartTransition()
    {
        Cam.SetTrigger("SceneTransition");
        Invoke("SwitchScene", 1f);
    }

    void SwitchScene()
    {
        //TempLevelManager.NextLevel();
        if(SceneManager.GetActiveScene().buildIndex != 9)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            TinySauce.OnGameFinished(PlayerPrefs.GetInt("Level", 1).ToString(), 1);
            PlayerPrefs.SetInt("Level", PlayerPrefs.GetInt("Level", 1) + 1);
            TinySauce.OnGameStarted(PlayerPrefs.GetInt("Level", 1).ToString());
        }
        else
        {
            SceneManager.LoadScene(0);
            TinySauce.OnGameFinished(PlayerPrefs.GetInt("Level", 1).ToString(), 1);
            PlayerPrefs.SetInt("Level", PlayerPrefs.GetInt("Level", 1) + 1);
            TinySauce.OnGameStarted(PlayerPrefs.GetInt("Level", 1).ToString());
        }
    }
}
