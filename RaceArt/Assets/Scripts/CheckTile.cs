﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CheckTile : MonoBehaviour
{
    //[HideInInspector]
    public GameObject Cache;
    //[HideInInspector]
    public Material Color;
    Vector3 TilePos;
    CheckForWin Main;
    Tilemap MainTilemap;

    private void Start()
    {   
        Main = GameObject.Find("GameManager").GetComponent<CheckForWin>();
        MainTilemap = Main.Map;
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag != "Tile") { return; }
        //Debug.Log("fd");
        Cache = other.gameObject;
        TilePos = Cache.transform.position;
        Color = other.gameObject.GetComponent<MeshRenderer>().material;
        Main.Check(Color, TilePos);
    }
}
