﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TempLevelManager : MonoBehaviour
{
    void Start()
    {
        GameObject.Find("Canvas").transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText("Level " + PlayerPrefs.GetInt("Level", 1));
        if (PlayerPrefs.GetInt("Level", 1) != 1)
        {   
            if(SceneManager.GetActiveScene().buildIndex != ((PlayerPrefs.GetInt("Level") - 1)% 10)) 
            {
                SceneManager.LoadScene((PlayerPrefs.GetInt("Level") - 1) % 10); //-1 because I started at 1 for simplicity
                //Debug.Log("DWuib");
                return; 
            }
        }
        Debug.Log("!");
    }

    /*public static void NextLevel()
    {
        if (PlayerPrefs.GetInt("Level", 1) != 1)
        {
            if (PlayerPrefs.GetInt("Level") - 1 == SceneManager.GetActiveScene().buildIndex) { return; }
            //SceneManager.LoadScene((PlayerPrefs.GetInt("Level") - 1)%10); //-1 because I started at 1 for simplicity
        }
    }*/
}
