﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabTile : MonoBehaviour
{
    Transform MainCam;
    RaycastHit Hit;
    private void Start()
    {
        MainCam = Camera.main.transform;
    }
    void Update()
    {
        if(Input.touchCount > 0)
        {
            Grab();
        }
    }

    void Grab()
    {
        Touch touch = Input.GetTouch(0);
        Ray ray = Camera.main.ScreenPointToRay(touch.position);
        Physics.Raycast(ray, out Hit, 100f, ~0, QueryTriggerInteraction.Ignore);
        //Debug.DrawLine(MainCam.position, Hit.point);
        if (Hit.collider.gameObject.tag == "Tile")
        {
            Hit.collider.gameObject.GetComponent<Move>().enabled = true;
        }

    }
}
