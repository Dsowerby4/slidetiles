﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CheckForWin : MonoBehaviour
{   
    GameObject[] PatternChecks = new GameObject[9];
    RaycastHit[] Hits;
    public GameObject[] SolutionBlocks;
    Material[] Solution = new Material[9];
    bool done = false;

    //Tilemap
    public Tilemap Map;

    private void Awake()
    {
        for(int i = 0; i < SolutionBlocks.Count(); i++)
        {
            Solution[i] = SolutionBlocks[i].GetComponent<MeshRenderer>().material;
        }
    }
    public void Check(Material Color, Vector3 TilePos)
    {
        if (Color.ToString().Substring(0, 1) == Solution[0].ToString().Substring(0, 1) && !done)
        {
            FillPatternChecks(TilePos);
            for (int i = 0; i < Solution.Count(); i++)
            {
                if (PatternChecks[i].GetComponent<MeshRenderer>().material.ToString().Substring(0, 1) != Solution[i].ToString().Substring(0, 1))
                {
                    return;
                }
            }
            //Debug.Log("Win");
            done = true;
            Invoke("DestroyAllWrongTiles", 0.75f);
            Invoke("StartSceneTransition", 1.5f);
        }
    }

    void FillPatternChecks(Vector3 T)
    {
        //Brute Force because I'm Lazy
        //Raycasts 3*3 group
        Vector3 TP = new Vector3(T.x, T.y + 5, T.z);
        Hits = new RaycastHit[9];
        Physics.Raycast(new Vector3(TP.x, TP.y, TP.z), Vector3.down, out Hits[0], 10f, ~0, QueryTriggerInteraction.Ignore);
        Physics.Raycast(new Vector3(TP.x + 1, TP.y, TP.z), Vector3.down, out Hits[1], 10f, ~0, QueryTriggerInteraction.Ignore);
        Physics.Raycast(new Vector3(TP.x + 2, TP.y, TP.z), Vector3.down, out Hits[2], 10f, ~0, QueryTriggerInteraction.Ignore);
        Physics.Raycast(new Vector3(TP.x, TP.y, TP.z - 1), Vector3.down, out Hits[3], 10f, ~0, QueryTriggerInteraction.Ignore);
        Physics.Raycast(new Vector3(TP.x + 1, TP.y, TP.z - 1), Vector3.down, out Hits[4], 10f, ~0, QueryTriggerInteraction.Ignore);
        Physics.Raycast(new Vector3(TP.x + 2, TP.y, TP.z - 1), Vector3.down, out Hits[5], 10f, ~0, QueryTriggerInteraction.Ignore);
        Physics.Raycast(new Vector3(TP.x, TP.y, TP.z - 2), Vector3.down, out Hits[6], 10f, ~0, QueryTriggerInteraction.Ignore);
        Physics.Raycast(new Vector3(TP.x + 1, TP.y, TP.z - 2), Vector3.down, out Hits[7], 10f, ~0, QueryTriggerInteraction.Ignore);
        Physics.Raycast(new Vector3(TP.x + 2, TP.y, TP.z - 2), Vector3.down, out Hits[8], 10f, ~0, QueryTriggerInteraction.Ignore);
        //Convert Hits to Triggers
        for (int i = 0; i < Hits.Count(); i++)
        {   
            //if(!Hits[i].collider.gameObject.GetComponent<Collider>()) { return; }
            PatternChecks[i] = Hits[i].collider.gameObject;
            
            //Debug.Log(PatternChecks[i].transform.position);
        }
        
    }

    //Why not :)
    void DestroyAllWrongTiles()
    {
        List<Transform> Children = new List<Transform>();
        for(int i = 0; i < Map.transform.childCount; i++)
        {
            Children.Add(Map.transform.GetChild(i));
        }
        foreach(Transform child in Children)
        {
            if(child.CompareTag("Tile") && !PatternChecks.Contains(child.gameObject))
            {
                Transform G = child.GetChild(0);
                G.parent = null;
                G.GetComponent<ParticleSystemRenderer>().material = child.GetComponent<MeshRenderer>().material;
                G.GetComponent<ParticleSystem>().Play();
                Destroy(child.gameObject);
            }
        }
    }

    void StartSceneTransition()
    {
        GameObject.Find("GameManager").GetComponent<TransitionManager>().StartTransition();
    }
}
